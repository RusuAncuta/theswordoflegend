package ro;

import java.io.*;
import java.nio.charset.StandardCharsets;

public class FileReader {

    public static void readFile() throws IOException {
        File intro_file = new File("src/main/resources/Intro.txt");
        FileInputStream read_file = new FileInputStream(intro_file);
        InputStreamReader isr=new InputStreamReader(read_file, StandardCharsets.UTF_8);
        BufferedReader br = new BufferedReader(isr);
        String line;
        while ((line = br.readLine()) != null) {
            System.out.println(line);


        }
    }
}
