package ro;

import ro.Character.CharacterSheet;
import ro.Character.Players;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;



public class GameEngine {

    public static void main(String[] args) throws IOException {

        //Enter data using BufferReader
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        FileReader.readFile();
        System.out.println("-------------------------------------------------------------------------------------------------------------------------------------------");

        while (true) {
            System.out.println("Menu:1. Start Game \n\t" +
                    " 2. Quit Game\n\t" +
                    " 3. Save progress");
            String user_input = reader.readLine();
            if (user_input.equals("2")) {
                break;
            }
            // if (user_input=="3"){SAVE}
            if (user_input.equals("1")) {
                System.out.println("Please set a name for your player\n");
                String user_name = reader.readLine();
                System.out.println("PLAYER: " + Players.buildPlayer1(user_name).getName() +
                        "\t \tHEALTH: " + Utils.calculateHealthPercentage(user_name)+
                        "\t \tINFLICTED DAMAGE: "+Players.buildPlayer1(user_name).getATTACK_DAMAGE()+"HP"
                        +"\t \t INVENTORY (tap I and check what items you posses) ");
                String input_I=reader.readLine();
                CharacterSheet.displayInventory(user_name);
               


            }

        }

    }
}
