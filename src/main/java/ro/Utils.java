package ro;

import org.junit.Test;
import ro.Character.Character;
import ro.Character.Players;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Utils {

    public static String calculateHealthPercentage(String user_input) throws IOException {
        String health;
        if (Players.buildPlayer1(user_input).getHealth() == Players.buildPlayer1(user_input).getSTAMINA()) {
            health = "Full";
        } else
            health = ((double) Players.buildPlayer1(user_input).getHealth() / Players.buildPlayer1(user_input).getSTAMINA()) * 100 + "%";
        health = health.replace(".0", "");
        return health;
    }

    public static int setPlayerDamage() {
        List<Integer> damageList = new ArrayList<>();
        damageList.add(20);
        damageList.add(25);
        damageList.add(30);
        damageList.add(35);
        Random rd = new Random();
        int index = rd.nextInt(damageList.size());
        int damage;
        return damage = damageList.get(index);
    }

    public static Character getRandom(List<Character> characters, List<Integer> weights) {
        Random rd = new Random();
        int rd_1 = rd.nextInt(100);
        int sum = 0;
        List<Integer> normalizedWeights = new ArrayList<>();
        for (int i = 0; i < weights.size(); i++) {
            sum += weights.get(i);
            normalizedWeights.add(sum);
        }
        int i;
        for (i = 0; i < normalizedWeights.size(); i++) {
            if (rd_1 <= normalizedWeights.get(i)) {
                break;
            }

        }
        i = Math.min(normalizedWeights.size() - 1, i);
        return characters.get(i);

    }

}
