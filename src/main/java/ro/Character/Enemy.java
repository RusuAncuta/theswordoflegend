package ro.Character;

import java.util.ArrayList;
import java.util.List;

public class Enemy {

    public static List<Character> enemyList() {
        List<Character> enemyList = new ArrayList<>();
        Character skeleton_warrior, serpent_Aphopis, Alastor, Mortimer;


        enemyList.add(new Character.Builder()
                .withName("Skeleton_Warrior")
                .withHealth(40)
                .withSTAMINA(40)
                .withATTACK_DAMAGE(10)
                .build());
        enemyList.add(new Character.Builder()
                .withName("Serpent Aphopis")
                .withHealth(50)
                .withSTAMINA(50)
                .withATTACK_DAMAGE(5)
                .build());
        enemyList.add(new Character.Builder()
                .withName("Alastor The Persecutor")
                .withHealth(60)
                .withSTAMINA(60)
                .withATTACK_DAMAGE(15)
                .build());
        enemyList.add(new Character.Builder()
                .withName("Mortimer_Breath Of death")
                .withHealth(70)
                .withSTAMINA(70)
                .withATTACK_DAMAGE(20)
                .build());
            return enemyList;
    }
}
