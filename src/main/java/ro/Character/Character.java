package ro.Character;


public class Character {
    private int health;
    private int STAMINA;
    private int ATTACK_DAMAGE;
    private int gold;
    private boolean venom_potion;
    private boolean armor;
    private String name;
    private String ancient_stone;

    public int getHealth() {
        return health;
    }

    public int getSTAMINA() {
        return STAMINA;
    }

    public int getATTACK_DAMAGE() {
        return ATTACK_DAMAGE;
    }

    public int getGold() {
        return gold;
    }

    public boolean getVenom_potion() {
        return venom_potion;
    }

    public String getName() {
        return name;
    }

    public boolean getArmor() {
        return armor;
    }
    public String getAncient_stone() {
        return ancient_stone;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public void setSTAMINA(int STAMINA) {
        this.STAMINA = STAMINA;
    }

    public void setATTACK_DAMAGE(int ATTACK_DAMAGE) {
        this.ATTACK_DAMAGE = ATTACK_DAMAGE;
    }

    public void setGold(int gold) {
        this.gold = gold;
    }

    public void setVenom_potion(boolean venom_potion) {
        this.venom_potion = venom_potion;
    }

    public void setArmor(boolean armor) {
        this.armor = armor;
    }
    public void setAncient_stone(String ancient_stone) {
        this.ancient_stone = ancient_stone;
    }

    public void setName(String user_name) {
        this.name = name;
    }

    public Character(Builder builder) {
        this.health = builder.health;
        this.STAMINA = builder.STAMINA;
        this.ATTACK_DAMAGE = builder.ATTACK_DAMAGE;
        this.gold = builder.gold;
        this.venom_potion = builder.venom_potion;
        this.armor = builder.armor;
        this.name = builder.name;

    }

    public static class Builder {
        private int health;
        private int STAMINA;
        private int ATTACK_DAMAGE;
        private int gold;
        private boolean venom_potion;
        private boolean armor;
        private String name;

        public Builder withHealth(int health) {
            this.health = health;
            return this;
        }

        public Builder withSTAMINA(int STAMINA) {
            this.STAMINA = STAMINA;
            return this;
        }

        public Builder withATTACK_DAMAGE(int ATTACK_DAMAGE) {
            this.ATTACK_DAMAGE = ATTACK_DAMAGE;
            return this;
        }

        public Builder withGold(int gold) {
            this.gold = gold;
            return this;
        }

        public Builder withVenom_potion(boolean venom_potion) {
            this.venom_potion = venom_potion;
            return this;
        }

        public Builder withArmor(boolean armor) {
            this.armor = armor;
            return this;
        }

        public Builder withName(String name) {
            this.name = name;
            return this;
        }

        public Character build() {
            return new Character(this);
        }

    }
}
