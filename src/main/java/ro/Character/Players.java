package ro.Character;

import ro.Utils;

import java.io.IOException;

public class Players {

    public static Character buildPlayer1(String user_input) throws IOException {

                Character player_1= new Character.Builder()
                .withName(user_input)
                .withSTAMINA(100)
                .withHealth(100)
                .withATTACK_DAMAGE(Utils.setPlayerDamage())
                .withGold(0)
                .withArmor(true)
                .withVenom_potion(true)
                .build();
    return player_1;
    }
}
