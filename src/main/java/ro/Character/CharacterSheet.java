package ro.Character;

import ro.Character.Players;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;


public class CharacterSheet {

       public static void displayInventory(String user_input) throws IOException {
        //this is used to display to the player the items in his possesion and how/when he can use them
        System.out.println("INVENTORY: \n");
        System.out.println("\t GOLD: " + Players.buildPlayer1(user_input).getGold()
                + "\t with gold you can by different items that will help you defeat the enemy\n");
        if (Players.buildPlayer1(user_input).getVenom_potion()) {
            System.out.println("\t LIFE POTION: is the antidote against Serpent Aphopis' venom, " +
                    "\n\t or you can use it to restore 20 HP of you health." +
                    "\n\t Note: you can use the potion just once, so use it wisely!\n");
        }
        if (Players.buildPlayer1(user_input).getArmor()) {
            System.out.println("\t UPPER BODY ARMOUR: the armor will protect you form the enemy's attack " +
                    "\n\t and the damage inflicted will be diminished by 5HP\n");
        }



    }


}


