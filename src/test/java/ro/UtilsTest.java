package ro;

import org.junit.Test;
import ro.Character.Character;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class UtilsTest {

    @Test
    public void testGetRandomReturnNonNullValue() {
        List<Character> characters = new ArrayList<>();
        characters.add(new Character.Builder().build());
        characters.add(new Character.Builder().build());
        List<Integer> procentage = new ArrayList<>();
        procentage.add(70);
        procentage.add(20);
        procentage.add(5);
        Character random = Utils.getRandom(characters, procentage);
        assertNotNull(random);
    }

    @Test
    public void testGetRandomReturnsFirstElemWithCorrespondingWeight() {
        List<Character> characters = new ArrayList<>();
        characters.add(new Character.Builder().withName("A").build());
        characters.add(new Character.Builder().withName("B").build());
        characters.add(new Character.Builder().withName("C").build());
        List<Integer> procentage = new ArrayList<>();
        procentage.add(70);
        procentage.add(20);
        procentage.add(10);
        int count=0;
        int iterations = 100_000;
        int maxVal = (int) (0.73*iterations);
        int minVal =(int) (0.67*iterations);
        for(int i = 0; i< iterations; i++){
            Character randomCharacter = Utils.getRandom(characters, procentage);
            if (randomCharacter.getName().equals("A")){
                count++;
            }
        }
        System.out.println(count);
        assertTrue(count<= maxVal);
        assertTrue(count> minVal);
    }

    @Test
    public void testGetRandomWorksWellWithIncompletesWeights() {
        List<Character> characters = new ArrayList<>();
        characters.add(new Character.Builder().withName("A").build());
        characters.add(new Character.Builder().withName("B").build());
        characters.add(new Character.Builder().withName("C").build());
        List<Integer> procentage = new ArrayList<>();
        procentage.add(70);
        procentage.add(20);
        procentage.add(5);
        int count=0;
        int iterations = 100_000;
        int maxVal = (int) (0.73*iterations);
        int minVal =(int) (0.67*iterations);
        for(int i = 0; i< iterations; i++){
            Character randomCharacter = Utils.getRandom(characters, procentage);
            if (randomCharacter.getName().equals("A")){
                count++;
            }
        }
        System.out.println(count);
        assertTrue(count<= maxVal);
        assertTrue(count> minVal);
    }
}