# TheSwordOfLegend

The sword of legend

The game features has: main menu, save and load capabilities.

Actions:
During the game, the character has the following actions: explore, attack, sleep, escape, open.

Actions only appear in certain contexts. For example attack only appears during an encounter.

Sleep: The character restores 20% of full health.

Attack: Character deals damage equal to the strength

Explore: 20% chance of encountering an enemy, 5% chance of finding an item, 75% chance nothing happens

Escape: Enemy is no longer attacking the character

Open: Retrieves the content of a chest

Open character sheet: show character description (available in all contexts)

Open inventory: show gold (available in all contexts)

Character sheet
A character sheet includes: health, stamina, strength

Health: how many HP points the character currently has

Stamina: max HP points

Strength: damage done when attacking

Contexts
Available actions: explore, open character sheet

Encounter

Available actions: attack, escape.

An enemy is encountered.

If enemy is not dead, return to this state.

If the enemy is killed, obtain x gold coins.

Nothing happens

Available actions: Sleep, explore.

There is no enemy or treasure

Chest found

Available actions: open, explore.

X gold is found. Random number between 1 and 5.

Implementation suggestions
Make use of properties files for configuring initial values of the game, like max enemy life, max character life etc.

Save progress into files and load from files (progress = health & gold, so far)


Guidelines
Game should make use of many data structures and OOP principles.
Start by making a graph of states (contexts). The actions carry from one state to another.
Requirements are not necessarily complete, so either ask for clarification or improvise :)
Incremental updates to the game will come as much as time allows (using items, adding possibility to evade attacks, potions).
Group projects are allowed.
Unit tests are mandatory for testing states and actions.

